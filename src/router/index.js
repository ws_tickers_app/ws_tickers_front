import VueRouter from 'vue-router'
import Home from '../views/HomeView.vue'
const routes = [
    {
        path: '/',
        name: 'HomeView',
        component: Home
    },
    {
        path: '/tickers',
        component: () => import('../views/TickersView.vue'),
        children: [
            {
                path: '',
                name: 'DefaultGraph',
                component: () => import('../components/DefaultGraph.vue')
            },
            {
                path: ':ticker_id',
                name: 'TickerGraph',
                component: () => import('../components/TickerGraph.vue')
            }]
    },

    {
        path: '/news',
        name: 'NewsView',
        component: () => import('../views/NewsView.vue')
    }
]

const router = new VueRouter({
    routes,
    mode: "history",
});

export default router