import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import VueNativeSock from 'vue-native-websocket'
import router from './router';
import VueRouter from "vue-router";
import VuePageTransition from 'vue-page-transition'



Vue.config.productionTip = false
Vue.use(VueNativeSock, 'ws://localhost:8000/ticker/ticker_33/stream')
Vue.use(VueRouter)
Vue.use(VuePageTransition)


new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
