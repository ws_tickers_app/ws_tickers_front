import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'font-awesome/css/font-awesome.min.css'

Vue.use(Vuetify)

const vuetify = new Vuetify({
    theme: {
        options: {
            customProperties: true
        },
        dark: true,
        themes: {
            dark: {
                primary: '#FF9800',
                secondary: '#b0bec5',
                anchor: '#8c9eff',
            },
            light: {
                primary: '#FF9800',
                card: '#f0ece6',
            },
        },
    },
})

export default vuetify
